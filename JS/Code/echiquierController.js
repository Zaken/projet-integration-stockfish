/**
 * controleur de l'échiquier
 */

class EchiquierController {

    /**
     * constructeur d'échiquier
     */
    constructor() {

        /**
         * booleen pour l'affichage du plateau
         */
        this.estInverse = false;

        /**
         * temps d'attente entre chaque coup de stockfish (
         */
        this.dureeAttente = 500;

        /**
         * plateau de l'échiquier
         * @type {Plateau}
         * @private
         */
        this._plateau = new Plateau(false);

        /**
         * si true alors on affiche les meilleurs coups proposés par stockfish sinon on ne fait rien
         * @type {boolean}
         */
        this.affichagePropositionStockfish = false;
        /**
         * vue de l'échiquier
         * @type {EchiquierView}
         */
        this.view = new EchiquierView();
        this.view.afficherEchiquier(this._plateau.getTabPieces());
        this.view.initView();

        /** position de la pièce qui peut être déplacé au prochain tour (via action à 3 paramètres + peut être vide */
        this._posPieceProp = null;

        /**
         * joueur blanc
         * @type {Joueur}
         * @private
         */
        this._joueurBlanc = new Joueur(true);

        /**
         * joueur noir
         * @type {Joueur}
         * @private
         */
        this._joueurNoir = new Joueur(false);

        /**
         * enregistrement des propositions de stockfish
         * dans l'instance courante
         * @type {Array}
         */
        this.propositionStockfish = [];
        this.propositionStockfish[0] = new Pos(0, 0);
        this.propositionStockfish[1] = new Pos(0, 0);

        /**
         * profondeur de l'arbre créé par stockfish
         * 5 par défaut mais peut être modifié (entre 1 et 20 pour que le calcul soit rapide)
         * @type {number}
         * @private
         */
        this._profondeur = 10;

        let self = this;
        document.getElementById("echiquier").addEventListener("click", function () {
            self.action(self._plateau._tour, self.view.posSelect.x, self.view.posSelect.y);
        });

        // désactivation de l'action par défaut du clic gauche
        window.oncontextmenu = (e) => {
            e.preventDefault();
        }

        this._positionAppui = null;
        // récupération du plateau
        const para = document.querySelector('table');
        // event click droit appui
        para.onpointerdown = (event) => {
            if (event.button === 2) {
                // sauvegarde de la case cliqué
                this._positionAppui = new Pos(Number(event.target.id[0]), Number(event.target.id[1]));
            }
        };
        // récupération click droit relachement
        para.onpointerup = (event) => {
            if (event.button === 2) {
                let x = event.target.id[0];
                let y = event.target.id[1];
                (this._positionAppui.x == x && this._positionAppui.y == y) ? this.view.dessinerProposition(x, y) :
                    this.view.dessinerProposition(this._positionAppui.x, this._positionAppui.y, x, y);
            }
        };

        this.stockfish = new Worker('stockfish.js');
        self = this;

        //Ajout du répertoire d'ouvertures book.bin

        // var bookRequest = new XMLHttpRequest();
        // bookRequest.open('GET', 'book.bin', true);
        // bookRequest.responseType = "arraybuffer";
        // bookRequest.onload = function(event) {
        //   self.request(bookRequest, event);
        // };
        // bookRequest.send(null);

        this.stockfish.onmessage = function (event) {
            self.recupDataFlowStockfish(event);
        };
        this.communicationStockfish();
        this.communicationStockfish();

        /**
         * gestion des boutons/ txtbox (tout le graphique)
         */

        /**
         * liste à puce de la profondeur
         * @type {HTMLElement}
         */
        let profondeur = document.getElementById("listeProfondeur");
        for (let i = 1; i <= 20; i++) {
            let option = document.createElement("option");
            option.text = i;
            option.value = i;
            profondeur.add(option);
        }

        /**
         * bouton valide associé uniquement à la liste de profondeur
         * @type {HTMLElement}
         */
        let valider = document.getElementById("btnValider");
        valider.onclick = function () {
            self.setProfondeur(document.getElementById("listeProfondeur").value)
        }

        /**
         * gestion du bouton permettant de recommencer une partie
         * @type {HTMLElement}
         */
        let recommencer = document.getElementById("btnRecommencer");
        recommencer.onclick = function () {
            self.reinitialiser();
        }

        /**
         * gestion du bouton permettant d'inverser graphiquement l'échiquier
         * @type {HTMLElement}
         */
        let inverser = document.getElementById("btnInverser")
        inverser.onclick = function () {
            self.view.tournerEchiquier(self.estInverse);
            self.estInverse = !(self.estInverse);
        }

        let choixJoueur = document.getElementById("btnValiderJoueur");
        choixJoueur.onclick = function () {
            self.traitementChoixGestionJoueur();
        }

        let txtValiderDuree = document.getElementById("txtValiderDuree");
        txtValiderDuree.value = this.dureeAttente;
        let btnValiderDuree = document.getElementById("btnValiderDuree");
        btnValiderDuree.onclick = function () {
            if (txtValiderDuree.value.match("^\\d{1,4}$")) {
                self.dureeAttente = Number(txtValiderDuree.value);
            } else {
                txtValiderDuree.value = self.dureeAttente;
            }
        }
        let btnAfficherPropStockfish = document.getElementById("btnAfficherPropStockfish");
        btnAfficherPropStockfish.onclick = function () {
            self.affichagePropositionStockfish = !(self.affichagePropositionStockfish);
            self.communicationStockfish();
        }

        /**
         * Chaine de caractères de l'historique
         */

        this._historique = "";
    }

    /**
     * gestion et traitements des communications
     * entre stockfish et l'application
     * @param event l'objet envoyé par stockfish
     */
    recupDataFlowStockfish(event) {
        let temp = event.data;
        // console.log("data : " + event.data);
        EchiquierView.updateEchiquier(this._plateau.getTabPieces());

        if (temp.substring(10, 14) != "none" && temp.substring(0, 8) === "bestmove" && this._plateau.nulle == false) {

            let fromY = FENToPos(temp[9])
            let fromX = FENToPos(temp[10]);
            let toY = FENToPos(temp[11]);
            let toX = FENToPos(temp[12]);

            if (this.propositionStockfish[0].y != fromY &&
                this.propositionStockfish[0].x != fromX) {
                this.propositionStockfish[0].y = fromY;
                this.propositionStockfish[0].x = fromX;
                this.propositionStockfish[1].y = toY;
                this.propositionStockfish[1].x = toX;
            }

            if (!(this._joueurBlanc.estVraiJoueur) || !(this._joueurNoir.estVraiJoueur)) {
                sleep(this.dureeAttente);
            }

            if (this._plateau._tour) {
                if (this._joueurBlanc.estVraiJoueur) {
                    if (this.affichagePropositionStockfish) {
                        this.view.afficherPropStockfish(fromX, fromY, toX, toY);
                    }
                } else {
                    this.action(this._plateau._tour, fromX, fromY, toX, toY);
                }
            } else {
                if (this._joueurNoir.estVraiJoueur) {
                    if (this.affichagePropositionStockfish) {
                        this.view.afficherPropStockfish(fromX, fromY, toX, toY);
                    }
                } else {
                    this.action(this._plateau._tour, fromX, fromY, toX, toY);
                }
            }
        } else {
            let msg = "";
            let divFin = document.getElementById("divFinDeJeu");
            let txtFin = document.getElementById("txtFinDeJeu");

            if(this._plateau.nulle){
                msg = "Partie nulle";
            } else if (temp.substring(10, 14) == "none") {
                // vérification pour savoir qui a gagné
                if (this._plateau.echecBlanc) {
                    console.log("noir gagne");
                    msg = "Noir a gagné";
                } else if (this._plateau.echecNoir) {
                    console.log("blanc gagne");
                    msg = "Blanc a gagné";
                }
                if (txtFin.innerHTML == "") {
                    this.action(this._plateau._tour, 0, 0);
                }
            }
            // blocage du plateau graphique
            if (msg !== "") {
                divFin.style.zIndex = "1";
                txtFin.innerHTML = msg;
            }
        }
    }

    request(bookRequest, event) {
        if (bookRequest.status == 200) {
            this.stockfish.postMessage({book: bookRequest.response});
        }
    }

    /**
     * réinitialise la plateau de jeu
     */
    reinitialiser() {
        let txtFin = document.getElementById("txtFinDeJeu");
        let divFin = document.getElementById("divFinDeJeu");
        txtFin.innerHTML = "";
        this._plateau = new Plateau(false);
        this._historique = "";
        this.view.updateHistorique(this._historique);
        EchiquierView.updateEchiquier(this._plateau.getTabPieces());
        this.communicationStockfish();
        divFin.style.zIndex = "-1";
    }

    /**
     * déplacement souhaitée par un joueur
     * envoi des données au plateau
     * @param fromX pos en x de la pièce à déplacer si possible
     * @param fromY pos en y de la pièce à déplacer si possible
     * @param toX pos possible de la nouvelle position de la pièce
     * @param toY pos possible de la nouvelle position de la pièce
     * @param tour la couleur du joueur qui doit jouer
     */
    action(tour, fromX, fromY, toX, toY) {
        //console.log("fen courant : " + (tabToFEN(this._plateau)));
        // récupération d'une liste de coup si toX et toY sont non vides et d'un boolean sinon
        let res = this._plateau.regles(tour, fromX, fromY, toX, toY);
        // (toujours à false car pas de drag and drop) => pas de proposition de coups avec 2 positions
        if (res == true) {
            // console.log("passage res = true");
            // appel avec les 5 paramètres valides
            this.miseAJourHistorique(fromX, fromY, toX, toY);
            this._plateau.nettoyerProposition();
            this._posPieceProp = null;
            this._plateau._tour = !(this._plateau._tour);
        } else {
            // premier tour avec 3 paramètres, sauvegarde de la position si elle est valide
            // si la propositoon de coup possède des coups valides et que c'est bien son tour de jouer
            if (this._posPieceProp === null && res.length > 0 && this._plateau.getTabPieces()[fromX][fromY].getCouleur() === this._plateau._tour) {
                this._posPieceProp = new Pos(fromX, fromY);
                this._plateau.ajoutPropostion(res);
            } else {
                // second tour avec 3 paramètres, appel avec 5 paramètres : si le coup est valide avec la position sauvegardé (non null)
                if (this._posPieceProp !== null && this._plateau.regles(this._plateau._tour, this._posPieceProp.x, this._posPieceProp.y, fromX, fromY)) {
                    this.miseAJourHistorique(fromX, fromY, toX, toY);
                    this._plateau._tour = !(this._plateau._tour);
                }
                // dans tous les cas on supprime la position sauvegardée
                this._posPieceProp = null;
                this._plateau.nettoyerProposition();
            }
        }
        this.communicationStockfish();
        EchiquierView.updateEchiquier(this._plateau.getTabPieces());
        this.view.updateHistorique(this._historique);
    }

    /**
     * gestion communication avec stockfish
     * envoi du fen courant + précision de la profondeur
     */
    communicationStockfish() {
        // début gestion communication stockfish
        let msg = 'position fen ' + tabToFEN(this._plateau) + ' ' + this._plateau._privilegeFEN + ' ' + this._plateau._nbrCoupsRegle + ' ' + (Math.floor((this._plateau._nbrCoups + 1) / 2));
        console.log("execution avec : " + msg);
        if (this._joueurBlanc.estVraiJoueur || this._joueurNoir.estVraiJoueur) {
            this.stockfish.postMessage("go depth  1");
            this.stockfish.postMessage(msg);
        }
        this.stockfish.postMessage("go depth " + this._profondeur);
        this.stockfish.postMessage(msg);
        // fin gestion communication stockfish
    }

    traitementChoixGestionJoueur() {
        let choixJoueurBlanc = choixJoueurIA(true);
        let choixJoueurNoir = choixJoueurIA(false);
        this._joueurBlanc.estVraiJoueur = choixJoueurBlanc;
        this._joueurNoir.estVraiJoueur = choixJoueurNoir;
    }

    miseAJourHistorique(fromX, fromY, toX, toY) {
        if (this._posPieceProp == null) {
            if (this._plateau._tour) //joueur blanc
                this._historique += ((Math.floor(this._plateau._nbrCoups + 1) / 2) + ". " + formeUci(toX, toY, fromX, fromY) + " ");
            else
                this._historique += (formeUci(toX, toY, fromX, fromY) + " ");
        } else {
            if (this._plateau._tour) //joueur blanc
                this._historique += ((Math.floor(this._plateau._nbrCoups + 1) / 2) + ". " + formeUci(this._posPieceProp.x, this._posPieceProp.y, fromX, fromY) + " ");
            else
                this._historique += (formeUci(this._posPieceProp.x, this._posPieceProp.y, fromX, fromY) + " ");
        }
    }

    /**
     * getter du joueur blanc
     * @returns {Joueur}
     */
    getJoueurBlanc() {
        return this._joueurBlanc;
    }

    /**
     * getter du joueur noir
     * @returns {Joueur}
     */
    getJoueurNoir() {
        return this._joueurNoir;
    }

    /**
     * setter du joueur blanc
     * @param value
     */
    setJoueurBlanc(value) {
        this._joueurBlanc = value;
    }

    /**
     * setter du joueur noir
     * @param value
     */
    setJoueurNoir(value) {
        this._joueurNoir = value;
    }

    /**
     * getter du plateau
     * @returns {Plateau}
     */
    getPlateau() {
        return this._plateau;
    }

    /**
     * setter du plateau
     * @param value
     */
    setPlateau(value) {
        this._plateau = value;
    }

    getTour() {
        return this._plateau._tour;
    }

    setTour(value) {
        this._plateau._tour = value;
    }

    getPosPieceProp() {
        return this._posPieceProp;
    }

    setPosPieceProp(value) {
        this._posPieceProp = value;
    }

    setProfondeur(value) {
        this._profondeur = value;
    }
}

/**
 * récupére le choix pour un couleur entre joueur et IA
 * @param couleur boolean de la couleur (true == blanc, false == noir)
 * @return {boolean} vrai si un vrai joueur joue, sinon faux (IA)
 */
function choixJoueurIA(couleur) {
    if (couleur) {
        return document.getElementById("optionB1").classList.length === 3;
    } else {
        return document.getElementById("optionN1").classList.length === 3;
    }
}
