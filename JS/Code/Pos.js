/**
 * représentation d'un couple de positions x et y (strictement positives
 */
class Pos{

    /**
     * constructeur de Pos
     * @param x l'ordonée de la position
     * @param y l'abscisse de la position
     */
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    /**
    *comparateur de deux positions
    *@param pos
    *@return bool, true si les deux positions sont égales
    */
    equals(pos){
      return this.x === pos.x && this.y === pos.y;
    }
}
