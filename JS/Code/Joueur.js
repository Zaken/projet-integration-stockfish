/**
 * joueur sur l'échiquier
 */
class Joueur{

    /**
     * constructeur de joueur
     * @param couleur la couleur du jour
     */
    constructor(couleur){
        /**
         * couleur joué par le joueur
         */
        this._couleur = couleur;
        /**
         * est un vrai joueur si true sinon IA
         * @type {boolean}
         */
        this.estVraiJoueur = true;
    }

    getCouleur() {
        return this._couleur;
    }

    setCouleur(value) {
        this._couleur = value;
    }

}