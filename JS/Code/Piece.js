/**
 * représentation d'une pièce sur un jeu d'echec
 */

class Piece {

    /**
     * constructeur de pièce
     */
    constructor(couleur, type){
        /** couleur (true = blanc,false = noir) représentant la couleur de la pièce */
        this._couleur = couleur;
        /** type de la pièce cf outils.js*/
        this._type  = type;
        /** à vrai si la pièce courante fait partie des coups possibles du prochain tour  */
        this._estCoupPossible = false;
    }

    // getter de la couleur
    getCouleur() {
        return this._couleur;
    }

    // getter du type
    getType() {
        return this._type;
    }

    //getter de la lettre correspondante à la pièce correspondante
    getLettre(){
        switch (this._type) {
            case Type.VIDE : return " ";
            case Type.PION_NOIR: return "P";
            case Type.PION_BLANC: return "P";
            case Type.ROI: return "R";
            case Type.REINE: return "D";
            case Type.FOU: return "F";
            case Type.CAVALIER: return "C"; 
            case Type.TOUR: return "T";
        }
    }

    // /getter de la lettre correspondante à la pièce correspondante en anglais
    getLettreEN(){
        switch (this._type) {
            case Type.VIDE : return " ";
            case Type.PION_NOIR: return "p";
            case Type.PION_BLANC: return "p";
            case Type.ROI: return "k";
            case Type.REINE: return "q";
            case Type.FOU: return "b";
            case Type.CAVALIER: return "n";
            case Type.TOUR: return "r";
        }
    }

    getUrl(clr){
        if(this._type === Type.VIDE){
            return " ";
        }
        let debut = "../photos/", fin = ".png";
        if(clr){
            switch (this._type) {
                case Type.PION_BLANC: return (debut + "wP" + fin);
                case Type.ROI: return (debut + "wK" + fin);
                case Type.REINE: return (debut + "wQ" + fin);
                case Type.FOU: return (debut + "wB" + fin);
                case Type.CAVALIER: return (debut + "wN" + fin);
                case Type.TOUR: return (debut + "wR" + fin);
            }
        } else {
            switch (this._type) {
                case Type.PION_NOIR: return (debut + "bP" + fin);
                case Type.ROI: return (debut + "bK" + fin);
                case Type.REINE: return (debut + "bQ" + fin);
                case Type.FOU: return (debut + "bB" + fin);
                case Type.CAVALIER: return (debut + "bN" + fin);
                case Type.TOUR: return (debut + "bR" + fin);
            }
        }
    }

    // setter de la couleur
    setCouleur(clr){
        this._couleur = clr;
    }

    // setter du type
    setType(type){
        this._type = type;
    }

    getEstCoupPossible() {
        return this._estCoupPossible;
    }

    setEstCoupPossible(value) {
        this._estCoupPossible = value;
    }

}