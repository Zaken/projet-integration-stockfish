/*
 * fonctions d'affichage de l'échiquier
 */


/**
 * view de l'échiquier
 */
class EchiquierView {

    /**
     * constructeur de EchiquierView
     */
    constructor() {
        this.posSelect = new Pos(0, 0);
    }

    /*
     *fonction de historique
     */
    updateHistorique(historique){
       document.getElementById("texteHistorique").innerHTML = historique;
    }

    /**
     * affiche la totalité du tableau de pièces
     * ?amélioration : envoyer en param uniquement un format FEN
     * @param tabPieces le tableau des pièces du plateau
     */
    afficherEchiquier(tabPieces) {
        let table = document.createElement("table");
        table.classList.add("tableEchiquier");
        table.id = "echiquier"
        // document.write("<table class='tableEchiquier' id='echiquier'>");
        for (let i = 0; i < TAILLE; ++i) {
            // ligne
            let tr = document.createElement("tr");
            // document.write("<tr>");
            for (let j = 0; j < TAILLE; ++j) {
                // élément de la ligne
                let td = document.createElement("td");
                let position = i.toString() + j.toString();
                let cssClass = ((i + j) % 2 === 0 ? "caseBlanche" : "caseNoir");
                td.classList.add(cssClass);
                cssClass = "cell";
                td.classList.add(cssClass);
                td.id = position;
                // document.write("<td id='" + position + "' class= 'cell " + ((i + j) % 2 === 0 ? "caseBlanche" : "caseNoir") + "' ></td>");
                tr.appendChild(td);
            }
            table.appendChild(tr);
            // document.write("</tr>");
        }


        document.getElementById("panneauCentral").appendChild(table);
        // document.write("</table>");
        this.constructor.updateEchiquier(tabPieces);
    }

    /**
     * met à jour le tableau html
     * ?amélioration : envoyer en param uniquement un format FEN
     * @param tabPieces
     */
    static updateEchiquier(tabPieces) {
        // TODO utiliser estInverse
        let px = 75, pxMoins = 0;
        let table = document.getElementById("echiquier");
        for (let i = 0; i < TAILLE; i++) {
            for (let j = 0; j < TAILLE; j++) {
                if (tabPieces[i][j].getType() !== Type.VIDE && tabPieces[i][j].getCouleur() === false) {
                    if (!(table.rows[i].cells[j].firstChild)) {
                        // génération de l'image à afficher (noir)
                        let imgNoir = document.createElement('img');
                        imgNoir.id = i.toString() + j.toString();
                        imgNoir.src = tabPieces[i][j].getUrl(false);
                        imgNoir.width = px;
                        imgNoir.height = px;
                        if (tabPieces[i][j].getType() === Type.PION_NOIR || tabPieces[i][j].getType() === Type.PION_BLANC) {
                            imgNoir.width -= pxMoins;
                            imgNoir.height -= pxMoins;
                        }
                        table.rows[i].cells[j].appendChild(imgNoir);
                    } else {
                        table.rows[i].cells[j].removeChild(table.rows[i].cells[j].firstChild);
                        let imgNoir = document.createElement('img');
                        imgNoir.id = i.toString() + j.toString();
                        imgNoir.src = tabPieces[i][j].getUrl(false);
                        imgNoir.width = px;
                        imgNoir.height = px;
                        if (tabPieces[i][j].getType() === Type.PION_NOIR || tabPieces[i][j].getType() === Type.PION_BLANC) {
                            imgNoir.width -= pxMoins;
                            imgNoir.height -= pxMoins;
                        }
                        table.rows[i].cells[j].appendChild(imgNoir);
                    }
                } else if (tabPieces[i][j].getType() !== Type.VIDE && tabPieces[i][j].getCouleur() === true) {
                    // génération de l'image à afficher (blanche) si non vide
                    if (!(table.rows[i].cells[j].firstChild)) {
                        let imgBlanc = document.createElement('img');
                        imgBlanc.src = tabPieces[i][j].getUrl(true);
                        imgBlanc.id = i.toString() + j.toString();
                        imgBlanc.width = px;
                        imgBlanc.height = px;
                        if (tabPieces[i][j].getType() === Type.PION_NOIR || tabPieces[i][j].getType() === Type.PION_BLANC) {
                            imgBlanc.width -= pxMoins;
                            imgBlanc.height -= pxMoins;
                        }
                        table.rows[i].cells[j].appendChild(imgBlanc);
                    } else {
                        table.rows[i].cells[j].removeChild(table.rows[i].cells[j].firstChild);
                        let imgBlanc = document.createElement('img');
                        imgBlanc.id = i.toString() + j.toString();
                        imgBlanc.src = tabPieces[i][j].getUrl(true);
                        imgBlanc.width = px;
                        imgBlanc.height = px;
                        if (tabPieces[i][j].getType() === Type.PION_NOIR || tabPieces[i][j].getType() === Type.PION_BLANC) {
                            imgBlanc.width -= pxMoins;
                            imgBlanc.height -= pxMoins;
                        }
                        table.rows[i].cells[j].appendChild(imgBlanc);
                    }
                } else {
                    if (table.rows[i].cells[j].firstChild) {
                        table.rows[i].cells[j].removeChild(table.rows[i].cells[j].firstChild);
                    }
                }

                // verification coup proposé
                if (tabPieces[i][j].getEstCoupPossible()) {
                    if (tabPieces[i][j].getType() === Type.VIDE) {
                        let node = document.createElement("DIV");
                        table.rows[i].cells[j].appendChild(node);
                        // table.rows[i].cells[j].classList.add("cercleSolo");
                        table.rows[i].cells[j].firstChild.classList.add("cercleSolo");
                    } else {
                        table.rows[i].cells[j].firstChild.classList.add("cercle");
                    }
                }
            }
        }
    }

    /**
     * définit l'attribut onClick pour chaque cases du tableau
     */
    initView() {
        let self = this;
        let table = document.getElementById("echiquier");
        for (let i = 0; i < table.rows.length; i++) {
            for (let j = 0; j < table.rows[i].cells.length; j++)
                table.rows[i].cells[j].onclick = function () {
                    self.posSelect.x = i;
                    self.posSelect.y = j;
                };
        }
    }

    dessinerProposition(fromX, fromY, toX, toY){

        // appel si toX, toY non nul de dessinerProposition pour toutes les cases entre les 2 positions
        if(toX !== undefined && toY !== undefined){
            // aucune coordonnées similaires entre les 2 positions
            if(fromX != toX && fromY !== toY){
                let tmpX = fromX, tmpY = fromY, i = 0;
                // si une diagonale
                if(EchiquierView.memeDiagonale(fromX, fromY, toX, toY)){
                    let j = 0;

                    if(fromX < toX){
                        i = 1;
                    } else {
                        i = -1;
                    }

                    if(fromY < toY){
                        j = 1;
                    } else {
                        j = -1;
                    }

                    while(tmpX != toX){
                        this.dessinerProposition(tmpX, tmpY);
                        tmpX += i;
                        tmpY += j;
                    }
                    this.dessinerProposition(tmpX, tmpY);

                } else {
                    if(fromX < toX){
                        i = 1;
                    } else {
                        i = -1;
                    }

                    while(tmpX != toX){
                        this.dessinerProposition(tmpX, fromY);
                        tmpX += i;
                    }
                    this.dessinerProposition(tmpX, fromY);

                    if(fromY < toY){
                        i = 1;
                    } else {
                        i = -1;
                    }

                    while(tmpY != toY){
                        this.dessinerProposition(tmpX, tmpY);
                        tmpY += i;
                    }
                    this.dessinerProposition(tmpX, tmpY);
                }
            } else {
                // uniquement x différent
                if(fromX != toX){
                    let i = 0, tmpX = fromX;
                    if(fromX < toX){
                        i = 1;
                    } else {
                        i = -1;
                    }

                    while(tmpX != toX ){
                        this.dessinerProposition(tmpX, fromY);
                        tmpX += i;
                    }
                    this.dessinerProposition(tmpX, fromY);
                }

                // uniquement y différent
                if(fromY != toY){
                    let i = 0, tmpY = fromY;
                    if(fromY < toY){
                        i = 1;
                    } else {
                        i = -1;
                    }

                    while(tmpY != toY){
                        this.dessinerProposition(fromX, tmpY);
                        tmpY += i;
                    }
                    this.dessinerProposition(fromX, tmpY);
                }
            }
        }

        let table = document.getElementById("echiquier");
        let fromCercle = document.createElement('div');
        fromCercle.id = String(fromX) + String(fromY);
        fromCercle.classList.add("cercleProposition");

        if(table.rows[fromX].cells[fromY].childNodes.length !== 0 && table.rows[fromX].cells[fromY].childNodes[0].nodeName === "IMG"){
            fromCercle.style.width = "auto";
            fromCercle.style.height = "auto";
            let td = document.getElementById(String(fromX) + String(fromY));
            let piece = td.removeChild(td.childNodes[0]);
            fromCercle.appendChild(piece);
            td.appendChild(fromCercle);
        } else {
            if(table.rows[fromX].cells[fromY].childNodes.length === 0){
                table.rows[fromX].cells[fromY].appendChild(fromCercle);
            }
        }
    }

    afficherPropStockfish(fromX, fromY, toX, toY) {
        // console.log(fromX + ":" + fromY)
            let table = document.getElementById("echiquier");
            let fromCercle = document.createElement('div');
            fromCercle.id = String(fromX) + String(fromY);
            fromCercle.style.borderColor = "red";

            fromCercle.classList.add("cercleProposition");
            if (table.rows[fromX].cells[fromY].childNodes.length !== 0 && table.rows[fromX].cells[fromY].childNodes[0].nodeName === "IMG") {
                fromCercle.style.width = "auto";
                fromCercle.style.height = "auto";
                let td = document.getElementById(String(fromX) + String(fromY));
                let piece = td.removeChild(td.childNodes[0]);
                fromCercle.appendChild(piece);
                td.appendChild(fromCercle);
            } else {
                if (table.rows[fromX].cells[fromY].childNodes.length === 0) {
                    table.rows[fromX].cells[fromY].appendChild(fromCercle);
                }
            }
            if (typeof toX !== "undefined" && typeof toX !== "undefined") {
                this.afficherPropStockfish(toX, toY);
            }
    }

    tournerEchiquier(estTourner){
        let cssDefaut = "cell";
        let table = document.getElementById("echiquier");
        if(estTourner){
            // si deja tourné une fois (noir en bas)
            table.className = "tableEchiquier";
            for(let i = 0 ; i < TAILLE ; ++i){
                for(let j = 0 ; j < TAILLE ; ++j){
                    let aAjouterCss = ((i + j) % 2 === 0 ? "caseBlanche" : "caseNoir");
                    table.rows[i].cells[j].className = cssDefaut + " " + aAjouterCss;
                }
            }
        } else {
            // si pas encore tourné (blanc en bas)
            table.className += " rotate";
            for(let i = 0 ; i < TAILLE ; ++i){
                for(let j = 0 ; j < TAILLE ; ++j){
                    table.rows[i].cells[j].className += " rotate";
                }
            }
        }
    }

    static memeDiagonale(x1, y1, x2, y2) {
        return (Math.abs(x1 - x2) == Math.abs(y1 - y2));
    }

}