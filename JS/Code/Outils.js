let Type = {
    VIDE : 0,
    ROI : 1,
    REINE : 2,
    FOU : 3,
    CAVALIER : 4,
    TOUR : 5,
    PION_BLANC : 6,
    PION_NOIR : 7,
    LIAM : 8
};

/**
 * utilisation du format FEN pour la communication avecd stockfish
 * @return le format FEN du plateau courant sous la forme d'une chaine de caractère
 */
function tabToFEN(plateau){
    let chaineFEN = "", cpt = 0;

    //1ere partie : ajout des positions du plateau
    // parcours de chaque ligne
    for(let i = 0 ; i < TAILLE ; ++i){
        if(i !== 0){
            chaineFEN += "/";
        }
        for(let j = 0 ; j < TAILLE ; ++j){
            if(plateau._tabPieces[i][j].getType() !== Type.VIDE){
                if(cpt !== 0){
                    chaineFEN += cpt.toString();
                    cpt = 0;
                }
                if(plateau._tabPieces[i][j].getCouleur() === true){
                    chaineFEN += plateau._tabPieces[i][j].getLettreEN().toUpperCase();
                } else {
                    chaineFEN += plateau._tabPieces[i][j].getLettreEN();
                }
            } else {
                cpt++;
            }
        }
        if(cpt !== 0){
            chaineFEN += cpt.toString();
            cpt = 0;
        }
    }


    //2ème partie : ajout de la couleur du joueur courant

    chaineFEN += " ";
    if(plateau._tour){
      chaineFEN += "w";
    } else {
        chaineFEN += "b";
    }


    //3ème partie : les roques
    chaineFEN+= " ";
    if(!plateau._pRoqueBlanc && !plateau._gRoqueBlanc && !plateau._pRoqueNoir && !plateau._gRoqueNoir){
      chaineFEN += "-";
    } else {
      if(plateau._pRoqueBlanc){
        chaineFEN+="K";
      }
      if(plateau._gRoqueBlanc){
        chaineFEN+="Q";
      }
      if(plateau._pRoqueNoir){
        chaineFEN+="k";
      }
      if(plateau._gRoqueNoir){
        chaineFEN+="q";
      }
    }



    return chaineFEN;
}

/**
*transforme une chaine de caractère au format FEN (simplement la première partie) en tableau de Pièces
*@param string : chaine au format FEN
*@return Pieces[][], un tabPiece correspondant au format FEN
*amélioration : ajouter une couleur dans la chaine et la renvoyer également
*/

function FENToTab(fen){

  //Initialisation du tableau
    let test = new Plateau(true);

    let n = 0; // L'indice dans la boucle
    let i = 0; // dans le tableau
    let j = 0;

    while(n < fen.length && fen.charAt(n) != " "){
        switch(fen.charAt(n)){
            // Fin de la rangée
            case "/":
                j=0;
                i++;
                break;

            //Cases vides

            case "1":
                j++;
                break;
            case "2":
                j+=2;
                break;
            case "3":
                j+=3;
                break;
            case "4":
                j+=4;
                break;
            case "5":
                j+=5;
                break;
            case "6":
                j+=6;
                break;
            case "7":
                j+=7;
                break;
            case "8":
                j+=8;
                break;

            //Pièces blanches
            case "B":
                test._tabPieces[i][j].setCouleur(true);
                test._tabPieces[i][j].setType(3);
                j++;
                break;
            case "K":
                test._tabPieces[i][j].setCouleur(true);
                test._tabPieces[i][j].setType(1);
                j++;
                break;
            case "N":
                test._tabPieces[i][j].setCouleur(true);
                test._tabPieces[i][j].setType(4);
                j++;
                break;
            case "P":
                test._tabPieces[i][j].setCouleur(true);
                test._tabPieces[i][j].setType(6);
                j++;
                break;
            case "Q":
                test._tabPieces[i][j].setCouleur(true);
                test._tabPieces[i][j].setType(2);
                j++;
                break;
            case "R":
                test._tabPieces[i][j].setCouleur(true);
                test._tabPieces[i][j].setType(5);
                j++;
                break;

            //Pièces noires
            case "b":
                test._tabPieces[i][j].setCouleur(false);
                test._tabPieces[i][j].setType(3);
                j++;
                break;
            case "k":
                test._tabPieces[i][j].setCouleur(false);
                test._tabPieces[i][j].setType(1);
                j++;
                break;
            case "n":
                test._tabPieces[i][j].setCouleur(false);
                test._tabPieces[i][j].setType(4);
                j++;
                break;
            case "p":
                test._tabPieces[i][j].setCouleur(false);
                test._tabPieces[i][j].setType(7);
                j++;
                break;
            case "q":
                test._tabPieces[i][j].setCouleur(false);
                test._tabPieces[i][j].setType(2);
                j++;
                break;
            case "r":
                test._tabPieces[i][j].setCouleur(false);
                test._tabPieces[i][j].setType(5);
                j++;
                break;

        }
        n++;

    }
    n++;

    // Couleur du joueur courant
    if(fen.charAt(n) === "b"){
      test._tour = false;
    }
    if(fen.charAt(n) === "w"){
      test._tour = true;
    }
    n++;
    n++;


    //Roques
    if(fen.charAt(n) === "-"){
      n++;
      test._pRoqueBlanc = false;
      test._gRoqueBlanc = false;
      test._pRoqueNoir = false;
      test._gRoqueNoir = false;
    } else {

      if(fen.charAt(n) === "K"){
        test._pRoqueBlanc = true;
        n++;
      } else {
        test._pRoqueBlanc = false;
      }

      if(fen.charAt(n) === "Q"){
        test._gRoqueBlanc = true;
        n++;
      } else {
        test._gRoqueBlanc = false;
      }
      if(fen.charAt(n) === "k"){
        test._pRoqueNoir = true;
        n++;
      } else {
        test._pRoqueNoir = false;
      }
      if(fen.charAt(n) === "q"){
        test._gRoqueNoir = true;
        n++;
      } else {
        test._gRoqueNoir = false;
      }
    }

    n++;

    //Privilège
    if(fen.charAt(n) === "-"){
      n++;
      test._privilege = new Pos(-1, -1);
      test._privilegeFEN = "-";
    } else {
      test._privilege = new Pos(fen.charAt(n+1), fen.charAt(n));
      test._privilegeFEN = fen.charAt(n) + fen.charAt(n+1);
    }

    n++;

    // 50 coups
    let tmp = "";
    while(fen.charAt(n) != " " && n<fen.length ){
      tmp += fen.charAt(n);
      n++;
    }
    test._nbrCoupsRegle = new Number(tmp);

    n++;

    //Nombre de coups total
    tmp = "";
    while(n < fen.length){
      tmp += fen.charAt(n);
      n++;
    }
    test._nbrCoups = 2*(new Number(tmp));

  return test;
}


  /**
  * transforme une position du plateau en position au format FEN
  *@param Pos
  *@return string
  */

function posToFEN(pos){
  let fen = "";
  switch(pos.y) {
    case 0:
        fen+="a";
        break;

    case 1:
        fen+="b";
        break;

    case 2:
        fen+="c";
        break;

    case 3:
        fen+="d";
        break;

    case 4:
        fen+="e";
        break;

    case 5:
        fen+="f";
        break;

    case 6:
        fen+="g";
        break;

    case 7:
        fen+="h";
        break;

  }

  switch(pos.x){
      case 7:
          fen +="1";
          break;

      case 6:
          fen +="2";
          break;

      case 5:
          fen +="3";
          break;

      case 4:
          fen +="4";
          break;

      case 3:
          fen +="5";
          break;

      case 2:
          fen +="6";
          break;

      case 1:
          fen +="7";
          break;

      case 0:
          fen +="8";
          break;
        }
  return fen;
}


/**
 * traduction des positions de stockfish, caractère par caractère.
 * pour notre tableau 2 dimensions
 * @param chaine
 * @return {number|*}
 */

function FENToPos(chaine){

    switch(chaine){
        case "1":
            return 7;

        case "2":
            return 6;

        case "3":
            return 5;

        case "4":
            return 4;

        case "5":
            return 3;

        case "6":
            return 2;

        case "7":
            return 1;

        case "8":
            return 0;


        case "a":
            return 0;

        case "b":
            return 1;

        case "c":
            return 2;

        case "d":
            return 3;

        case "e":
            return 4;

        case "f":
            return 5;

        case "g":
            return 6;

        case "h":
            return 7;

    }
}

function formeUci(fromX, fromY,toX, toY) {
    //converstion du movement de la pièce sous le format uci afin de remplir l'historique des coups convenablement
    //les x sont des chiffres et les y des nombres
    //en ascii "a" est égale a 97;
    //on soustrait et on prend l'absolue  pour prendre le chiffre car le tableau a pour origine(0,0)le haut gauche


    return(String.fromCharCode(97+fromY)+(Math.abs(fromX-8))+String.fromCharCode(97+toY)+(Math.abs(toX-8)));

}

function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
            break;
        }
    }
}

function pPartieFEN(plateau){
    let chaineFEN = "", cpt = 0;
    for(let i = 0 ; i < TAILLE ; ++i){
        if(i !== 0){
            chaineFEN += "/";
        }
        for(let j = 0 ; j < TAILLE ; ++j){
            if(plateau._tabPieces[i][j].getType() !== Type.VIDE){
                if(cpt !== 0){
                    chaineFEN += cpt.toString();
                    cpt = 0;
                }
                if(plateau._tabPieces[i][j].getCouleur() === true){
                    chaineFEN += plateau._tabPieces[i][j].getLettreEN().toUpperCase();
                } else {
                    chaineFEN += plateau._tabPieces[i][j].getLettreEN();
                }
            } else {
                cpt++;
            }
        }
        if(cpt !== 0){
            chaineFEN += cpt.toString();
            cpt = 0;
        }
    }
    return chaineFEN;
}