/**
 * plateau d'un échiquier
 */

const TAILLE = 8;

/**
 * plateau d'un échiquier
 */

class Plateau {


  /**
  *Constructeur
  *le booléen empty permet de construire un plateau sans aucune pièce.
  */


    constructor(empty) {
        /** le tableau de pièces de l'échiquier */

        this._tabFEN = [];
        this._nbrCoups = 0; // Nombre de demi-coups de la partie
        this._nbrCoupsRegle = 0; // Nombre de demi-coups depuis une prise ou le déplacement d'un pion
        this._tour = true; // couleur du joueur courant : blanc = true, noir = false
        this._pRoqueBlanc = true; // Validité du petit roque blanc
        this._gRoqueBlanc = true; // Validité du grand roque blanc
        this._pRoqueNoir = true; // Validité du petit roque noir
        this._gRoqueNoir = true; // Validité du grand roque noir
        this._privilege = new Pos(-1, -1); // Position du pion s'il y'a eu un privilège
        this._privilegeFEN = "-"; // chaine de caractère utilisée dans le FEN, représentant le privilège
        this._cinquanteCoups = false;
        this._pat = false;
        this._mat = false;
        this.nulle = false;
        this.echecBlanc = false;
        this.echecNoir = false;

        //Initialisation du plateau
        this._tabPieces = []; // tableau contenant les pièces de l'échiquier
        for (let i = 0; i < TAILLE; ++i) {
            this._tabPieces[i] = [];
        }

        // Remplissage
        if(empty){
          for (let i = 0; i <= 7; ++i) {
              for (let j = 0; j <= 7; ++j) {
                  this._tabPieces[i][j] = new Piece(false, Type.VIDE);
              }
          }
        } else {
          // remplissage des noirs
          for (let i = 0; i < TAILLE; ++i) {
              this._tabPieces[1][i] = new Piece(false, Type.PION_NOIR);
          }
          this._tabPieces[0][0] = new Piece(false, Type.TOUR);
          this._tabPieces[0][1] = new Piece(false, Type.CAVALIER);
          this._tabPieces[0][2] = new Piece(false, Type.FOU);
          this._tabPieces[0][3] = new Piece(false, Type.REINE);
          this._tabPieces[0][4] = new Piece(false, Type.ROI);
          this._tabPieces[0][5] = new Piece(false, Type.FOU);
          this._tabPieces[0][6] = new Piece(false, Type.CAVALIER);
          this._tabPieces[0][7] = new Piece(false, Type.TOUR);

          // remplissage des blancs
          for (let i = 0; i < TAILLE; ++i) {
              this._tabPieces[6][i] = new Piece(true, Type.PION_BLANC);
          }
          this._tabPieces[7][0] = new Piece(true, Type.TOUR);
          this._tabPieces[7][1] = new Piece(true, Type.CAVALIER);
          this._tabPieces[7][2] = new Piece(true, Type.FOU);
          this._tabPieces[7][3] = new Piece(true, Type.REINE);
          this._tabPieces[7][4] = new Piece(true, Type.ROI);
          this._tabPieces[7][5] = new Piece(true, Type.FOU);
          this._tabPieces[7][6] = new Piece(true, Type.CAVALIER);
          this._tabPieces[7][7] = new Piece(true, Type.TOUR);

          // remplissage des vides
          for (let i = 2; i <= 5; ++i) {
              for (let j = 0; j <= 7; ++j) {
                  this._tabPieces[i][j] = new Piece(false, Type.VIDE);
              }
          }
        }
    }



     /**
     * fonction principale pour le déplacement d'une pièce, vérifie sa validité, puis sa légalité
     * @param fromX pos en x de l'ancienne pièce
     * @param fromY pos en y de l'ancienne pièce
     * @param toX pos possible de la nouvelle pièce
     * @param toY pos possible de la nouvelle pièce
     * @param tour la couleur du joueur courant
     * @return booléen, true si le coup est accepté et donc que le plateau est modifié, false sinon
     */

    regles(tour, fromX, fromY, toX, toY) {

        if(typeof toX === "undefined" && typeof toY === "undefined"){
            return this.listeCoups(fromX, fromY, tour);
        }

        let oldPlateau = FENToTab(tabToFEN(this)); // sera égal au plateau courant si le valide est activé
        let pion = false; // Booléen permettant de vérifier si un pion a été déplacé
        if(this._tabPieces[fromX][fromY].getType() === 6 || this._tabPieces[fromX][fromY].getType() === 7){
          pion = true;
        }
        let nbrPieces = this.getNbrPieces();
        let valide = false; // est égal à true que si le coup proposé est dans la liste de coups possible
        let legal = true;
        let tabBlanc = [];
        let tabNoir = [];


        //Si la case est vide
        if (this._tabPieces[fromX][fromY].getType() === Type.VIDE || this._tabPieces[fromX][fromY].getCouleur() !== tour) {
            return 0;
        }


        // Test de la validité

         // On crée la liste de coups possibles
        let liste = this.listeCoups(fromX, fromY, tour);




        let i = 0;
        while(i < liste.length && valide === false){
            if(liste[i].x === toX && liste[i].y === toY){
                valide = true;

                //Prise en passant
                if( (this._tabPieces[fromX][fromY].getType() === 6 || this._tabPieces[fromX][fromY].getType() === 7) && toY == FENToPos(this._privilegeFEN[0]) && toX == FENToPos(this._privilegeFEN[1])){
                  if(fromY == toY -1){
                    this._tabPieces[fromX][fromY+1].setType(0);
                    this._tabPieces[fromX][fromY+1].setCouleur(false);
                  } else if(fromY == toY +1){
                    this._tabPieces[fromX][fromY-1].setType(0);
                    this._tabPieces[fromX][fromY-1].setCouleur(false);
                  }
                }


                //Roques
                if(tour && this._pRoqueBlanc && this._tabPieces[fromX][fromY].getType() === 1 && this._tabPieces[fromX][fromY+1].getType() === 0 && this._tabPieces[fromX][fromY+2].getType() === 0 && toX == fromX && toY == fromY + 2){
                  Plateau.echanger(this._tabPieces[fromX][fromY], this._tabPieces[toX][toY]);
                  Plateau.echanger(this._tabPieces[7][7], this._tabPieces[7][5]);
                } else if(tour && this._gRoqueBlanc && this._tabPieces[fromX][fromY].getType() === 1 && this._tabPieces[fromX][fromY-1].getType() === 0 && this._tabPieces[fromX][fromY-2].getType() === 0 && toX == fromX && toY == fromY - 2){
                  Plateau.echanger(this._tabPieces[fromX][fromY], this._tabPieces[toX][toY]);
                  Plateau.echanger(this._tabPieces[7][0], this._tabPieces[7][3]);
                } else if(!tour && this._pRoqueNoir && this._tabPieces[fromX][fromY].getType() === 1 && this._tabPieces[fromX][fromY+1].getType() === 0 && this._tabPieces[fromX][fromY+2].getType() === 0 && toX == fromX && toY == fromY + 2){
                  Plateau.echanger(this._tabPieces[fromX][fromY], this._tabPieces[toX][toY]);
                  Plateau.echanger(this._tabPieces[0][7], this._tabPieces[0][5]);
                } else if(!tour & this._gRoqueNoir && this._tabPieces[fromX][fromY].getType() === 1 && this._tabPieces[fromX][fromY-1].getType() === 0 && this._tabPieces[fromX][fromY-2].getType() === 0 && toX == fromX && toY == fromY - 2){
                  Plateau.echanger(this._tabPieces[fromX][fromY], this._tabPieces[toX][toY]);
                  Plateau.echanger(this._tabPieces[0][0], this._tabPieces[0][3]);
                }

                //Déplacements normaux
                 else {
                    this._tabFEN.push(pPartieFEN(this));
                  Plateau.echanger(this._tabPieces[fromX][fromY], this._tabPieces[toX][toY]);
                }

            }
            i += 1;
        }

         for(let i = 0; i < this._tabFEN.length; i++){
             for(let j = i+1; j < this._tabFEN.length; j++){
                 for(let k = j+1; k < this._tabFEN.length; k++){
                     if(this._tabFEN[k] === this._tabFEN[j] && this._tabFEN[k] === this._tabFEN[i]){
                         this.nulle = true;
                     }
                 }
             }
         }

        //Promotion en dame

          for (let i = 0; i < 8; i++) {
            if(this._tabPieces[0][i].getType() === 6){
              this._tabPieces[0][i].setType(2);
              valide = true;
            } else if(this._tabPieces[7][i].getType() === 7){
              this._tabPieces[7][i].setType(2);
              valide = true;
            }
          }

          //nulle par matériel
         //Remplissage de tabBlanc et tabNoir, qui vont contenir le type de toutes les pièces du plateau pour chaque couleur
         for(let i = 0; i < 8; i++){
             for(let j = 0; j < 8; j++){
                 if(this.isOwn(i, j, tour) && tour){
                     tabBlanc.push(this._tabPieces[i][j].getType());
                     if(this._tabPieces[i][j].getType() === 3){
                         tabBlanc.pop();
                         if((i+j)%2 === 0){
                             tabBlanc.push(30);
                         }
                         if((i+j)%2 === 1){
                             tabBlanc.push(31);
                         }
                     }
                 }
                 if(this.isOpp(i, j, tour) && tour){
                     tabNoir.push(this._tabPieces[i][j].getType());
                     if(this._tabPieces[i][j].getType() === 3){
                         tabNoir.pop();
                         if((i+j)%2 === 0){
                             tabNoir.push(30);
                         }
                         if((i+j)%2 === 1){
                             tabNoir.push(31);
                         }
                     }
                 }
                 if(this.isOwn(i, j, !tour) && !tour){
                     tabNoir.push(this._tabPieces[i][j].getType());
                     if(this._tabPieces[i][j].getType() === 3){
                         tabNoir.pop();
                         if((i+j)%2 === 0){
                             tabNoir.push(30);
                         }
                         if((i+j)%2 === 1){
                             tabNoir.push(31);
                         }
                     }
                 }
                 if(this.isOpp(i, j, !tour) && !tour){
                     tabBlanc.push(this._tabPieces[i][j].getType());
                     if(this._tabPieces[i][j].getType() === 3){
                         tabBlanc.pop();
                         if((i+j)%2 === 0){
                             tabBlanc.push(30);
                         }
                         if((i+j)%2 === 1){
                             tabBlanc.push(31);
                         }
                     }
                 }
             }
         }

         //Vérification des conditions de nulle avec tabBlanc et tabNoir
         for(let i = 0; i < Math.max(tabBlanc.length, tabNoir.length); i++){
             if(tabBlanc.length === 1 && tabNoir.length === 1){
                 this.nulle = true;
             }
             if(tabBlanc.length === 2 && tabNoir.length === 1 && tabBlanc[i] === 4){
                 this.nulle = true;
             }

             if(tabBlanc.length === 2 && tabNoir.length === 1 && tabBlanc[i] === 3){
                 this.nulle = true;
             }

             if(tabNoir.length === 2 && tabBlanc.length === 1 && tabNoir[i] === 4){
                 this.nulle = true;
             }

             if(tabNoir.length === 2 && tabBlanc.length === 1 && tabNoir[i] === 3){
                 this.nulle = true;
             }

             if(tabNoir.length === 2 && tabBlanc.length === 2){
                 if(tabBlanc[i] === 30){
                     if(tabNoir[0] === 31 || tabNoir[1] === 31){
                         this.nulle = true;
                     }
                 }
                 if(tabNoir[i] === 30){
                     if(tabBlanc[0] === 31 || tabBlanc[1] === 31){
                         this.nulle = true;
                     }
                 }
                 if(tabBlanc[i] === 31){
                     if(tabNoir[0] === 30 || tabNoir[1] === 30){
                         this.nulle = true;
                     }
                 }
                 if(tabNoir[i] === 31){
                     if(tabBlanc[0] === 30 || tabBlanc[1] === 30){
                         this.nulle = true;
                     }
                 }
             }
         }

        //Test de la légalité

        let posRoiBlanc = this.getPosRoiBlanc(); // posiition initiale du roi blanc
        let posRoiNoir = this.getPosRoiNoir();// posiiton initiale du roi noir

        // les deux prochains if empêchent le coup de se jouer si le roi du joueur courant est en échec
        if(tour && valide){
          if(!(posRoiBlanc.equals(new Pos(-1, -1))) && !(this.peutAtteindre(posRoiBlanc.x, posRoiBlanc.y, tour).equals(new Pos(-1, -1)))){
            console.log("Echec", this.peutAtteindre(posRoiBlanc.x, posRoiBlanc.y, tour) );
              this.echecBlanc = true;


              // reset total du plateau sur sa dernière version
            this._tabPieces = oldPlateau._tabPieces;
            this._tour = oldPlateau._tour;
            legal = false;
          }
        }
        if(!tour && valide){
          if(!(posRoiNoir.equals(new Pos(-1, -1))) && !(this.peutAtteindre(posRoiNoir.x, posRoiNoir.y, tour).equals(new Pos(-1, -1)))){
            console.log("Echec", this.peutAtteindre(posRoiNoir.x, posRoiNoir.y, tour));
              this.echecNoir = true;

              // reset total du plateau sur sa dernière version
            this._tabPieces = oldPlateau._tabPieces;
            this._tour = oldPlateau._tour;
            legal = false;
          } else {
                console.log("noir pas en echec");
            }
        }







        //Si le coup est joué
        if(valide && legal){
          //Mise à jour du nombre de demi-coups
          this._nbrCoups++;
          // Mise à jour des roques

          if(this._tabPieces[7][4].getType() === 0){
            this._gRoqueBlanc = false;
            this._pRoqueBlanc = false;
          }
          if( !(this._tabPieces[7][0].getType() === 5 && this._tabPieces[7][0].getCouleur() === true)){
            this._gRoqueBlanc = false;
          }
          if(!(this._tabPieces[7][7].getType() === 5 && this._tabPieces[7][7].getCouleur() === true)){
            this._pRoqueBlanc = false;
          }

          if(this._tabPieces[0][4].getType() === 0){
            this._gRoqueNoir = false;
            this._pRoqueNoir = false;
          }
          if( !(this._tabPieces[0][0].getType() === 5 && this._tabPieces[0][0].getCouleur() === false)){
            this._gRoqueNoir = false;
          }
          if(!(this._tabPieces[0][7].getType() === 5 && this._tabPieces[0][7].getCouleur() === false)){
            this._pRoqueNoir = false;
          }

          // Mise à jour du privilège
          if(this._tabPieces[toX][toY].getType() === 6 && fromX == toX + 2){
            this._privilege = new Pos(toX, toY);
            this._privilegeFEN = posToFEN(new Pos(fromX-1, fromY));
          } else if(this._tabPieces[toX][toY].getType() === 7 && fromX == toX - 2){
            this._privilege = new Pos(toX, toY);
            this._privilegeFEN = posToFEN(new Pos(fromX+1, fromY));
          } else {
            this._privilege = new Pos(-1, -1);
            this._privilegeFEN = "-";
          }

          //Mise à jour de la règle des 50 coups
          if(pion || nbrPieces != this.getNbrPieces()){
           this._nbrCoupsRegle = 0;
         } else {
           this._nbrCoupsRegle++;
         }

         //Verification de fin de partie
         let test = false;

         let i = 0;
         let j = 0;

         while(i<8 && !test){
           while(j<8 && !test){
             if(!this.caseProtegee(i, j, !tour)){
               test = true;
             }
             j++;
           }
           i++;
         }
         if(test){
           if(tour && !(posRoiBlanc.equals(new Pos(-1, -1))) && this.caseProtegee(posRoiBlanc.x, posRoiNoir.y, tour)){
             this._mat = true;

           } else {
             this._pat = true;
           }
           if(!tour && !(posRoiNoir.equals(new Pos(-1, -1))) && this.caseProtegee(posRoiNoir.x, posRoiNoir.y, tour)){
             this._mat = true;

           } else {
             this._pat = true;
           }

         }

        }

        //return valide && legal
        return valide && legal;
     }

    /*
    *calcule et renvoie la liste des coups possibles pour un pièce donnée
    * @param fromX pos en x de l'ancienne pièce
    * @param fromY pos en y de l'ancienne pièce
    * @param tour la couleur du joueur courant
    * @return coupsPossibles, la liste de positions possibles depuis la position donnée
    */

    listeCoups(fromX, fromY, tour) {
        let coupsPossibles = [];
        // Variables nécessaires dans le switch pour les cas fous, tours et dame
        let ghost = true; // vraie si la pièce peut encore se déplacer
        let tempX = fromX; //Variables pour calculer les déplacements plus longs que juste +1/-1
        let tempY = fromY;
        switch (this._tabPieces[fromX][fromY].getType()) {
            case Type.PION_BLANC:
                if (fromX === 6 && !this.isOpp(fromX - 1, fromY, tour) && !this.isOpp(fromX - 2, fromY, tour) && !this.isOwn(fromX - 1, fromY, tour) && !this.isOwn(fromX - 2, fromY, tour)) {
                    coupsPossibles.push(new Pos(fromX - 2, fromY));
                } // Privilège
                if (fromX -1 >= 0 && !this.isOpp(fromX - 1, fromY, tour) && !this.isOwn(fromX - 1, fromY, tour)){
                    coupsPossibles.push(new Pos(fromX - 1, fromY));
                } // remonte de 1
                if (fromX - 1 >= 0 && fromY - 1 >= 0 && this.isOpp(fromX - 1, fromY - 1, tour) && !this.isOwn(fromX - 1, fromY - 1, tour)) {
                    coupsPossibles.push(new Pos(fromX - 1, fromY - 1));
                } // Prise à gauche depuis le point de vue blanc
                if (fromX - 1 >= 0 && fromY + 1 <= 7 && this.isOpp(fromX - 1, fromY + 1, tour) && !this.isOwn(fromX - 1, fromY + 1, tour)){
                    coupsPossibles.push(new Pos(fromX - 1, fromY + 1));
                } // Prise à droite depuis le point de vue blanc
                if(!(this._privilege.equals(new Pos(-1, -1))) && this._privilege.x == fromX){
                  if(this._privilege.y == fromY -1){
                    coupsPossibles.push(new Pos(fromX-1, fromY-1));
                  } else if(this._privilege.y == fromY +1){
                    coupsPossibles.push(new Pos(fromX-1, fromY+1));
                  }
                } // Prise en passant
                break;

            case Type.PION_NOIR:
                if (fromX === 1 && !this.isOpp(fromX + 1, fromY, tour) && !this.isOpp(fromX + 2, fromY, tour) && !this.isOwn(fromX + 1, fromY, tour) && !this.isOwn(fromX + 2, fromY, tour)) {
                    coupsPossibles.push(new Pos(fromX + 2, fromY));
                } // Privilège
                if (fromX + 1 <= 7 && !this.isOpp(fromX + 1, fromY, tour) && !this.isOwn(fromX + 1, fromY, tour)) {
                    coupsPossibles.push(new Pos(fromX + 1, fromY));
                } // descend de 1
                if (fromX + 1 <= 7 && fromY - 1 >= 0 && this.isOpp(fromX + 1, fromY - 1, tour) && !this.isOwn(fromX + 1, fromY - 1, tour)){
                    coupsPossibles.push(new Pos(fromX + 1, fromY - 1));
                } // Prise à gauche depuis le point de vue blanc
                if (fromX + 1 <= 7 && fromY + 1 <= 7 && this.isOpp(fromX + 1, fromY + 1, tour) && !this.isOwn(fromX + 1, fromY + 1, tour)) {
                    coupsPossibles.push(new Pos(fromX + 1, fromY + 1));
                } // Prise à droite depuis le point de vue blanc
                if(!(this._privilege.equals(new Pos(-1, -1))) && this._privilege.x == fromX){
                  if(this._privilege.y == fromY -1){
                    coupsPossibles.push(new Pos(fromX+1, fromY-1));
                  } else if(this._privilege.y == fromY +1){
                    coupsPossibles.push(new Pos(fromX+1, fromY+1));
                  }
                } // Prise en passant
                break;

            case Type.CAVALIER:
                if (fromX + 1 <= 7 && fromX +2 <= 7 && fromY + 1 <= 7 && (this.isOpp(fromX + 2, fromY + 1, tour) || !this.isOwn(fromX + 2, fromY + 1, tour))) {
                    coupsPossibles.push(new Pos(fromX + 2, fromY + 1));
                }
                if (fromX + 1 <= 7 && fromX +2 <= 7 && fromY -1 >= 0 && (this.isOpp(fromX + 2, fromY - 1, tour) || !this.isOwn(fromX + 2, fromY - 1, tour))) {
                    coupsPossibles.push(new Pos(fromX + 2, fromY - 1));
                }
                if (fromX + 1 <= 7 && fromY + 1 <= 7 && fromY +2 <= 7 && (this.isOpp(fromX + 1, fromY + 2, tour) || !this.isOwn(fromX + 1, fromY + 2, tour))) {
                    coupsPossibles.push(new Pos(fromX + 1, fromY + 2));
                }
                if (fromX + 1 <= 7 && fromY - 1 >= 0 && fromY -2 >= 0 && (this.isOpp(fromX + 1, fromY - 2, tour) || !this.isOwn(fromX + 1, fromY - 2, tour))) {
                    coupsPossibles.push(new Pos(fromX + 1, fromY - 2));
                }
                if (fromX - 1 >= 0 && fromX -2 >= 0 && fromY + 1 <= 7 && (this.isOpp(fromX - 2, fromY + 1, tour) || !this.isOwn(fromX - 2, fromY + 1, tour))) {
                    coupsPossibles.push(new Pos(fromX - 2, fromY + 1));
                }
                if (fromX - 1 >= 0 && fromX -2 >= 0 && fromY - 1 >= 0 && (this.isOpp(fromX - 2, fromY - 1, tour) || !this.isOwn(fromX - 2, fromY - 1, tour))) {
                    coupsPossibles.push(new Pos(fromX - 2, fromY - 1));
                }
                if (fromX -1 >= 0 && fromY -1 >= 0 && fromY -2 >= 0 &&  (this.isOpp(fromX - 1, fromY - 2, tour) || !this.isOwn(fromX - 1, fromY - 2, tour))) {
                    coupsPossibles.push(new Pos(fromX - 1, fromY - 2));
                }
                if (fromX -1 >= 0 && fromY + 1 <= 7 && fromY +2 <= 7 && (this.isOpp(fromX - 1, fromY + 2, tour) || !this.isOwn(fromX - 1, fromY + 2, tour))) {
                    coupsPossibles.push(new Pos(fromX - 1, fromY +2));
                }
                break;

            case Type.ROI:
                for(let i = 0; i < 3; i++){
                    for(let j = 0; j < 3; j++){
                        if(fromX -1 + i <= 7 && fromX -1 + i >= 0 && fromY -1 + j <= 7 && fromY -1 + j >= 0 && !this.isOwn(fromX -1 + i, fromY -1 +j, tour)){
                            coupsPossibles.push(new Pos(fromX-1+i, fromY-1+j));
                        }
                    }
                }
                //Roques
                if(tour && this._pRoqueBlanc && this._tabPieces[7][6].getType() === 0 &&  this.caseProtegee(fromX, fromY, tour) && this.caseProtegee(fromX, fromY +1, tour) && this.caseProtegee(fromX, fromY +2, tour) && this._tabPieces[fromX][fromY+1].getType() == 0 && this._tabPieces[fromX][fromY +2].getType() == 0){
                  coupsPossibles.push(new Pos(fromX, fromY+2));
                }
                if(tour && this._gRoqueBlanc && this._tabPieces[7][2].getType() === 0 && this.caseProtegee(fromX, fromY, tour) && this.caseProtegee(fromX, fromY -1, tour) && this.caseProtegee(fromX, fromY -2, tour) && this._tabPieces[fromX][fromY-1].getType() == 0 && this._tabPieces[fromX][fromY-2].getType() == 0){
                  coupsPossibles.push(new Pos(fromX, fromY-2));
                }
                if(!tour && this._pRoqueNoir && this._tabPieces[0][6].getType() === 0 && this.caseProtegee(fromX, fromY, tour) && this.caseProtegee(fromX, fromY +1, tour) && this.caseProtegee(fromX, fromY +2, tour) && this._tabPieces[fromX][fromY+1].getType() == 0 && this._tabPieces[fromX][fromY+2].getType() == 0){
                  coupsPossibles.push(new Pos(fromX, fromY+2));
                }
                if(!tour && this._pRoqueBlanc && this._tabPieces[0][2].getType() === 0 && this.caseProtegee(fromX, fromY, tour) && this.caseProtegee(fromX, fromY -1, tour) && this.caseProtegee(fromX, fromY -2, tour) && this._tabPieces[fromX][fromY-1].getType() == 0 && this._tabPieces[fromX][fromY-2].getType() == 0){
                  coupsPossibles.push(new Pos(fromX, fromY-2));
                }

                break;

              case Type.FOU:
                //  4 cas pour chaque direction prise par le fou, on reset les 3 variables entre chaque while
                ghost = true;
                tempX = fromX;
                tempY = fromY;
                // Cas #1 vers en bas à droite du point de vue blanc
                while (ghost && tempX+1 <=7 && tempY+1 <=7 && !this.isOwn(tempX+1, tempY+1, tour)){
                  if(this.isOpp(tempX+1, tempY+1, tour)){
                    ghost = false;
                    }
                  coupsPossibles.push(new Pos(tempX+1, tempY+1));
                  tempX++;
                  tempY++;
                }
                //Cas #2 vers en bas à gauche du point de vue blanc
                ghost = true;
                tempX = fromX;
                tempY = fromY;
                while (ghost && tempX+1 <=7 && tempY-1 >= 0 && !this.isOwn(tempX+1, tempY-1, tour)){
                  if(this.isOpp(tempX+1, tempY-1, tour)){
                    ghost = false;
                    }
                  coupsPossibles.push(new Pos(tempX+1, tempY-1));
                  tempX++;
                  tempY--;
                }
                ghost = true;
                tempX = fromX;
                tempY = fromY;
                //Cas # 3 vers en haut à droite du point de vue blanc
                while (ghost && tempX-1 >=0 && tempY+1 <=7 && !this.isOwn(tempX-1, tempY+1, tour)){
                  if(this.isOpp(tempX-1, tempY+1, tour)){
                    ghost = false;
                    }
                  coupsPossibles.push(new Pos(tempX-1, tempY+1));
                  tempX--;
                  tempY++;
                }
                ghost = true;
                tempX = fromX;
                tempY = fromY;
                //Cas #4 vers en haut à gauche du point de vue blanc
                while (ghost && tempX-1 >=0 && tempY-1 >=0 && !this.isOwn(tempX-1, tempY-1, tour)){
                  if(this.isOpp(tempX-1, tempY-1, tour)){
                    ghost = false;
                    }
                  coupsPossibles.push(new Pos(tempX-1, tempY-1));
                  tempX--;
                  tempY--;
                }
                ghost = true;
                tempX = fromX;
                tempY = fromY;
                break;

              case Type.TOUR:
                 ghost = true;
                 tempX = fromX;
                 tempY = fromY;
                 //Cas #1 vers le bas du point de vue blancs
                  while (ghost && tempX+1 <=7 && !this.isOwn(tempX+1, tempY, tour)){
                    if(this.isOpp(tempX+1, tempY, tour)){
                      ghost = false;
                    }
                    coupsPossibles.push(new Pos(tempX+1, tempY));
                    tempX++;
                  }
                  ghost = true;
                  tempX = fromX;
                  tempY = fromY;
                  //Cas #2 vers le haut du point de vue blanc
                  while (ghost && tempX-1 >=0 && !this.isOwn(tempX-1, tempY, tour)){
                    if(this.isOpp(tempX-1, tempY, tour)){
                      ghost = false;
                    }
                    coupsPossibles.push(new Pos(tempX-1, tempY));
                    tempX--;
                  }
                  ghost = true;
                  tempX = fromX;
                  tempY = fromY;
                  //cas #3 vers la droite du point de vue blanc
                  while (ghost && tempY+1 <=7 && !this.isOwn(tempX, tempY+1, tour)){
                    if(this.isOpp(tempX, tempY+1, tour)){
                      ghost = false;
                    }
                    coupsPossibles.push(new Pos(tempX, tempY+1));
                    tempY++;
                  }
                  ghost = true;
                  tempX = fromX;
                  tempY = fromY;
                  //Cas 4 vers la gauche du point de vue blanc
                  while (ghost && tempY-1 >=0 && !this.isOwn(tempX, tempY-1, tour)){
                    if(this.isOpp(tempX, tempY-1, tour)){
                      ghost = false;
                    }
                    coupsPossibles.push(new Pos(tempX, tempY-1));
                    tempY--;
                  }
                  ghost = true;
                  tempX = fromX;
                  tempY = fromY;
                  break;

              case Type.REINE:
          //  8 cas pour chaque direction prise par la dame, on reset les 3 variables entre chaque while
                ghost = true;
                tempX = fromX;
                tempY = fromY;
            // Cas #1 vers en bas à droite du point de vue blanc
                while (ghost && tempX+1 <=7 && tempY+1 <=7 && !this.isOwn(tempX+1, tempY+1, tour)){
                  if(this.isOpp(tempX+1, tempY+1, tour)){
                    ghost = false;
                    }
                  coupsPossibles.push(new Pos(tempX+1, tempY+1));
                  tempX++;
                  tempY++;
                }
                ghost = true;
                tempX = fromX;
                tempY = fromY;
              //Cas #2 vers en bas à gauche du point de vue blanc
                while (ghost && tempX+1 <=7 && tempY-1 >= 0 && !this.isOwn(tempX+1, tempY-1, tour)){
                  if(this.isOpp(tempX+1, tempY-1, tour)){
                    ghost = false;
                    }
                  coupsPossibles.push(new Pos(tempX+1, tempY-1));
                  tempX++;
                  tempY--;
                }
                ghost = true;
                tempX = fromX;
                tempY = fromY;
          //Cas # 3 vers en haut à droite du point de vue blanc
              while (ghost && tempX-1 >=0 && tempY+1 <=7 && !this.isOwn(tempX-1, tempY+1, tour)){
                if(this.isOpp(tempX-1, tempY+1, tour)){
                  ghost = false;
                }
                coupsPossibles.push(new Pos(tempX-1, tempY+1));
                tempX--;
                tempY++;
              }
              ghost = true;
              tempX = fromX;
              tempY = fromY;
          //Cas #4 vers en haut à gauche du point de vue blanc
            while (ghost && tempX-1 >=0 && tempY-1 >=0 && !this.isOwn(tempX-1, tempY-1, tour)){
              if(this.isOpp(tempX-1, tempY-1, tour)){
                ghost = false;
              }
            coupsPossibles.push(new Pos(tempX-1, tempY-1));
            tempX--;
            tempY--;
            }
            ghost = true;
            tempX = fromX;
            tempY = fromY;
          //Cas #5 vers le bas du point de vue blancs
           while (ghost && tempX+1 <=7 && !this.isOwn(tempX+1, tempY, tour)){
             if(this.isOpp(tempX+1, tempY, tour)){
               ghost = false;
             }
             coupsPossibles.push(new Pos(tempX+1, tempY));
             tempX++;
           }
           ghost = true;
           tempX = fromX;
           tempY = fromY;
           //Cas #6 vers le haut du point de vue blanc
           while (ghost && tempX-1 >=0 && !this.isOwn(tempX-1, tempY, tour)){
             if(this.isOpp(tempX-1, tempY, tour)){
               ghost = false;
             }
             coupsPossibles.push(new Pos(tempX-1, tempY));
             tempX--;
           }
           ghost = true;
           tempX = fromX;
           tempY = fromY;
           //Cas #7 vers la droite du point de vue blanc
           while (ghost && tempY+1 <=7 && !this.isOwn(tempX, tempY+1, tour)){
             if(this.isOpp(tempX, tempY+1, tour)){
               ghost = false;
             }
             coupsPossibles.push(new Pos(tempX, tempY+1));
             tempY++;
           }
           ghost = true;
           tempX = fromX;
           tempY = fromY;
           //Cas 8 vers la gauche du point de vue blanc
           while (ghost && tempY-1 >=0 && !this.isOwn(tempX, tempY-1, tour)){
             if(this.isOpp(tempX, tempY-1, tour)){
               ghost = false;
             }
             coupsPossibles.push(new Pos(tempX, tempY-1));
             tempY--;
           }
           ghost = true;
           tempX = fromX;
           tempY = fromY;
     }

     let posRoiBlanc = this.getPosRoiBlanc(); // posiition initiale du roi blanc
     let posRoiNoir = this.getPosRoiNoir();// posiiton initiale du roi noir

        if(!posRoiNoir.equals(new Pos(-1, -1))){
            this.echecNoir = !(this.caseProtegee(posRoiNoir.x, posRoiNoir.y, false));
        }
        if(!posRoiBlanc.equals(new Pos(-1, -1))){
            this.echecBlanc = !(this.caseProtegee(posRoiBlanc.x, posRoiBlanc.y, true));
        }

     return coupsPossibles;
 }

 /**
 *détermine si une pièce à la position x, y peut être atteinte par une pièce adversaire
 *@param x, sa position en x
 *@param y, sa position en y
 *@param tour, la couleur du joueur courant
 *@return pos(resX, resY), jeu de positions de la première pièce qui peut atteindre la position donnée
 */
 peutAtteindre(x, y, tour){
   let resPos = new Pos(-1, -1);

   if (resPos.equals(new Pos(-1, -1))){
     resPos = this.peutAtteindreTour(x, y, tour);
   }
   if (resPos.equals(new Pos(-1, -1))){
     resPos = this.peutAtteindreCavalier(x, y, tour);
   }
   if (resPos.equals(new Pos(-1, -1))){
     resPos = this.peutAtteindreFou(x, y, tour);
   }
   if (resPos.equals(new Pos(-1, -1))){
     resPos = this.peutAtteindreRoi(x, y, tour);
   }
   if (resPos.equals(new Pos(-1, -1))){

     resPos = this.peutAtteindreReine(x, y, tour);
   }
   if (resPos.equals(new Pos(-1, -1)) && tour){
     resPos = this.peutAtteindrePionNoir(x, y, tour);
   }
   if (resPos.equals(new Pos(-1, -1)) && !tour){
     resPos = this.peutAtteindrePionBlanc(x, y, tour);
   }
   return resPos;
 }

 /**
 *même fonction que peutAtteindre, renvoie un booléen à la place + si la case est vide
 *@param x, sa position en x
 *@param y, sa position en y
 *@param tour, la couleur du joueur courant
 *@return boolean
 */

 caseProtegee(x, y, tour){
   return(this.peutAtteindre(x, y, tour).equals(new Pos(-1, -1)) );
 }
 /**
 *détermine si la position x, y peut être atteinte par une tour adverse
 *@param x, sa position en x
 *@param y, sa position en y
 *@param tour, la couleur du joueur courant
 *@return pos(resX, resY), jeu de positions de la première tour qui peut atteindre la position donnée
 */

   peutAtteindreTour(x, y, tour){
     let ghost  = true;
     let test = false;
     let tempX = x;
     let tempY = y;
     let resX = -1;
     let resY = -1;

      while (!test && ghost && tempX+1 <=7 && !this.isOwn(tempX+1, tempY, tour)){
        if(this.isOpp(tempX+1, tempY, tour)){
          ghost = false;
        }
        if(this.isOpp(tempX+1, tempY, tour) && this._tabPieces[tempX+1][tempY].getType() ===  5){
          test = true;
          resX = tempX + 1;
          resY = tempY;
        }
        tempX++;
      }
      ghost = true;
      tempX = x;
      while (!test && ghost && tempX-1 >=0 && !this.isOwn(tempX-1, tempY, tour)){

        if(this.isOpp(tempX-1, tempY, tour)){
          ghost = false;
        }
        if(this.isOpp(tempX-1, tempY, tour) && this._tabPieces[tempX-1][tempY].getType() === 5){
          test = true;
          resX = tempX - 1;
          resY = tempY;
        }
        tempX--;
      }
      ghost = true;
      tempX = x;
      while (!test && ghost && tempY-1 >=0 && !this.isOwn(tempX, tempY-1, tour)){
        if(this.isOpp(tempX, tempY-1, tour)){
          ghost = false;
        }
        if(this.isOpp(tempX, tempY-1, tour) && this._tabPieces[tempX][tempY-1].getType() === 5){
          test = true;
          resX = tempX;
          resY = tempY - 1;
        }
        tempY--;
      }
      tempY = y;
      ghost = true;
      while (!test && ghost && tempY+1 <=7 && !this.isOwn(tempX, tempY+1, tour)){
        if(this.isOpp(tempX, tempY+1, tour)){
          ghost = false;
        }
        if(this.isOpp(tempX, tempY+1, tour) && this._tabPieces[tempX][tempY+1].getType() === 5){
          test = true;
          resX = tempX;
          resY = tempY + 1;
        }
        tempY++;
      }
      tempY = y;
      ghost = true;

      return new Pos(resX, resY);
    }

    /**
    *détermine si la position x, y peut être atteinte par un fou adverse
    *@param x, sa position en x
    *@param y, sa position en y
    *@param tour, la couleur du joueur courant
    *@return pos(resX, resY), jeu de positions du premier fou qui peut atteindre la position donnée
    */

      peutAtteindreFou(x, y, tour){
       let ghost  = true;
       let test = false;
       let tempX = x;
       let tempY = y;
       let resX = -1;
       let resY = -1;
        while (!test && ghost && tempX+1 <=7 && tempY+1 <= 7 && !this.isOwn(tempX+1, tempY+1, tour)){
          if(this.isOpp(tempX+1, tempY+1, tour)){
            ghost = false;
          }
          if(this.isOpp(tempX+1, tempY+1, tour) && this._tabPieces[tempX+1][tempY+1].getType() ===  3){
            test = true;
            resX = tempX+1;
            resY = tempY+1;
          }
          tempX++;
          tempY++;
        }
       tempX = x;
       tempY = y;
       ghost = true;
        while (!test && ghost && tempX-1 >=0 && tempY+1 <= 7 && !this.isOwn(tempX-1, tempY+1, tour)){
          if(this.isOpp(tempX-1, tempY+1, tour)){
            ghost = false;
          }
          if(this.isOpp(tempX-1, tempY+1, tour) && this._tabPieces[tempX-1][tempY+1].getType() ===  3){
            test = true;
            resX = tempX-1;
            resY = tempY+1;
          }
          tempX--;
          tempY++;
        }
       tempX = x;
       tempY = y;
       ghost = true;
        while (!test && ghost && tempX+1 <=7 && tempY-1 >=0 && !this.isOwn(tempX+1, tempY-1, tour)){
          if(this.isOpp(tempX+1, tempY-1, tour)){
            ghost = false;
          }
          if(this.isOpp(tempX+1, tempY-1, tour) && this._tabPieces[tempX+1][tempY-1].getType() ===  3){
            test = true;
            resX = tempX+1;
            resY = tempY-1;
          }
          tempX++;
          tempY--;
        }
       tempX = x;
       tempY = y;
       ghost = true;
        while (!test && ghost && tempX-1 >=0 && tempY-1 >= 0 && !this.isOwn(tempX-1, tempY-1, tour)){
          if(this.isOpp(tempX-1, tempY-1, tour)){
            ghost = false;
          }
          if(this.isOpp(tempX-1, tempY-1, tour) && this._tabPieces[tempX-1][tempY-1].getType() ===  3){
            test = true;
            resX = tempX-1;
            resY = tempY-1;
          }
          tempX--;
          tempY--;
        }
        return new Pos(resX, resY);
    }

    /**
    *détermine si la position x, y peut être atteinte par un cavalier adverse
    *@param x, sa position en x
    *@param y, sa position en y
    *@param tour, la couleur du joueur courant
    *@return pos(resX, resY), jeu de positions du premier cavalier qui peut atteindre la position donnée
    */
      peutAtteindreCavalier(x, y, tour){
       let test = false;
       let tempX = x;
       let tempY = y;
       let resX = -1;
       let resY = -1;
                  if (tempX + 1 <= 7 && tempX +2 <= 7 && tempY + 1 <= 7 && this.isOpp(tempX+2, tempY+1, tour) && (this._tabPieces[tempX+2][tempY+1].getType() ===  4)) {
                     test = true;
                     resX = tempX+2;
                     resY = tempY+1
                  }
                  if (tempX + 1 <= 7 && tempX +2 <= 7 && tempY - 1 >= 0 && this.isOpp(tempX +2, tempY -1, tour) && (this._tabPieces[tempX+2][tempY-1].getType() ===  4)) {
                     test = true;
                     resX = tempX+2;
                     resY = tempY-1;
                  }
                  if (tempX + 1 <= 7 && tempY + 1 <= 7 && tempY +2 <= 7 && this.isOpp(tempX+1, tempY+2, tour) && (this._tabPieces[tempX+1][tempY+2].getType() ===  4)) {
                     test = true;
                     resX = tempX+1;
                     resY = tempY+2;
                  }
                  if (tempX + 1 <= 7 && tempY - 1 >= 0 && tempY -2 >= 0 && this.isOpp(tempX+1, tempY-2, tour) && (this._tabPieces[tempX+1][tempY-2].getType() ===  4)) {
                    test = true;
                    resX = tempX+1;
                    resY = tempY-2;
                  }
                  if (tempX - 1 >= 0 && tempX -2 >= 0 && tempY + 1 <= 7 && this.isOpp(tempX-2, tempY+1, tour) && (this._tabPieces[tempX-2][tempY+1].getType() ===  4)) {
                    test = true;
                    resX = tempX-2;
                    resY = tempY+1;
                  }
                  if (tempX - 1 >= 0 && tempX -2 >= 0 && tempY - 1 >= 0 && this.isOpp(tempX-2, tempY-1, tour) && (this._tabPieces[tempX-2][tempY-1].getType() ===  4)) {
                     test = true;
                     resX = tempX-2;
                     resY = tempY-1;
                  }
                  if (tempX - 1 >= 0 && tempY -1 >= 0 && tempY -2 >= 0 &&  this.isOpp(tempX-1, tempY-2, tour) && (this._tabPieces[tempX-1][tempY-2].getType() ===  4)) {
                     test = true;
                     resX = tempX-1;
                     resY = tempY-2;
                  }
                  if (tempX - 1 >= 0 && tempY + 1 <= 7 && tempY +2 <= 7 && this.isOpp(tempX-1, tempY+2, tour) && (this._tabPieces[tempX-1][tempY+2].getType() ===  4)) {
                     test = true;
                     resX = tempX-1;
                     resY = tempY+2;
             }
             return new Pos(resX, resY);
  }

     peutAtteindrePionNoir(x, y, tour){
          let test = false;
          let tempX = x;
          let tempY = y;
          let resX = -1;
          let resY = -1;
          if (tempX - 1 >= 0 && tempY - 1 >= 0 && this.isOpp(tempX-1, tempY-1, tour) && (this._tabPieces[tempX-1][tempY-1].getType() ===  7)){
                              test = true;
                              resX = tempX-1;
                              resY = tempY-1;
                          }
          if (tempX - 1 >= 0 && tempY + 1 <= 7 && this.isOpp(tempX-1, tempY+1, tour) && (this._tabPieces[tempX-1][tempY+1].getType() ===  7)) {
                              test = true;
                              resX = tempX-1;
                              resY = tempY+1;
                          }
          return new Pos(resX, resY);

      }

     peutAtteindrePionBlanc(x, y, tour){
          let test = false;
          let tempX = x;
          let tempY = y;
          let resX = -1;
          let resY = -1;

          if (tempX + 1 <= 7 && tempY - 1 >= 0 && this.isOpp(tempX+1, tempY-1, tour) && (this._tabPieces[tempX +1][tempY -1].getType() ===  6)){
                          test = true;
                          resX = tempX+1;
                          resY = tempY-1;
                      }
          if (tempX + 1 <= 7 && tempY + 1 <= 7 && this.isOpp(tempX+1, tempY+1, tour) && (this._tabPieces[tempX +1][tempY +1].getType() ===  6)) {
                          test = true;
                          resX = tempX+1;
                          resY = tempY+1;
                      }
          return new Pos(resX, resY);
     }

     peutAtteindreReine(x, y, tour){
       let test = false;
       let ghost = true;
       let tempX = x;
       let tempY = y;
       let resX = -1;
       let resY = -1;
       while (!test && ghost && tempX+1 <=7 && !this.isOwn(tempX+1, tempY, tour)){
         if(this.isOpp(tempX+1, tempY, tour)){
           ghost = false;
         }
         if(this.isOpp(tempX+1, tempY, tour) && this._tabPieces[tempX+1][tempY].getType() ===  2){
           test = true;
           resX = tempX + 1;
           resY = tempY;
         }
         tempX++;
       }
       ghost = true;
       tempX = x;
       while (!test && ghost && tempX-1 >=0 && !this.isOwn(tempX-1, tempY, tour)){
         if(this.isOpp(tempX-1, tempY, tour)){
           ghost = false;
         }
         if(this.isOpp(tempX-1, tempY, tour) && this._tabPieces[tempX-1][tempY].getType() === 2){
           test = true;
           resX = tempX - 1;
           resY = tempY;
         }
         tempX--;
       }
       ghost = true;
       tempX = x;
       while (!test && ghost && tempY-1 >=0 && !this.isOwn(tempX, tempY-1, tour)){
         if(this.isOpp(tempX, tempY-1, tour)){
           ghost = false;
         }
         if(this.isOpp(tempX, tempY-1, tour) && this._tabPieces[tempX][tempY-1].getType() === 2){
           test = true;
           resX = tempX;
           resY = tempY - 1;
         }
         tempY--;
       }
       tempY = y;
       ghost = true;
       while (!test && ghost && tempY+1 <=7 && !this.isOwn(tempX, tempY+1, tour)){
         if(this.isOpp(tempX, tempY+1, tour)){
           ghost = false;
         }
         if(this.isOpp(tempX, tempY+1, tour) && this._tabPieces[tempX][tempY+1].getType() === 2){
           test = true;
           resX = tempX;
           resY = tempY+1;
         }
         tempY++;
       }
       tempX = x;
       tempY = y;
       ghost = true;
       while (!test && ghost && tempX+1 <=7 && tempY+1 <= 7 && !this.isOwn(tempX+1, tempY+1, tour)){
         if(this.isOpp(tempX+1, tempY+1, tour)){
           ghost = false;
         }
         if(this.isOpp(tempX+1, tempY+1, tour) && this._tabPieces[tempX+1][tempY+1].getType() ===  2){
           test = true;
           resX = tempX+1;
           resY = tempY+1;
         }
         tempX++;
         tempY++;
       }
      tempX = x;
      tempY = y;
      ghost = true;
       while (!test && ghost && tempX-1 >=0 && tempY+1 <= 7 && !this.isOwn(tempX-1, tempY+1, tour)){
         if(this.isOpp(tempX-1, tempY+1, tour)){
           ghost = false;
         }
         if(this.isOpp(tempX-1, tempY+1, tour) && this._tabPieces[tempX-1][tempY+1].getType() ===  2){
           test = true;
           resX = tempX-1;
           resY = tempY+1;
         }
         tempX--;
         tempY++;
       }
      tempX = x;
      tempY = y;
      ghost = true;
       while (!test && ghost && tempX+1 <=7 && tempY-1 >=0 && !this.isOwn(tempX+1, tempY-1, tour)){
         if(this.isOpp(tempX+1, tempY-1, tour)){
           ghost = false;
         }
         if(this.isOpp(tempX+1, tempY-1, tour) && this._tabPieces[tempX+1][tempY-1].getType() ===  2){
           test = true;
           resX = tempX+1;
           resY = tempY-1;
         }
         tempX++;
         tempY--;
       }
      tempX = x;
      tempY = y;
      ghost = true;
       while (!test && ghost && tempX-1 >=0 && tempY-1 >= 0 && !this.isOwn(tempX-1, tempY-1, tour)){
         if(this.isOpp(tempX-1, tempY-1, tour)){
           ghost = false;
         }
         if(this.isOpp(tempX-1, tempY-1, tour) && this._tabPieces[tempX-1][tempY-1].getType() ===  2){
           test = true;
           resX = tempX-1;
           resY = tempY-1;
         }
         tempX--;
         tempY--;
       }
       return new Pos(resX, resY);

     }

     peutAtteindreRoi(x, y, tour){
       let test = false;
       let tempX = x;
       let tempY = y;
       let resX = -1;
       let resY = -1;
       for(let i = 0; i < 3; i++){
           for(let j = 0; j < 3; j++){
               if( tempX -1 + i <= 7 && tempX -1 + i >= 0 && tempY -1 + j <= 7 && tempY -1 + j >= 0 && this._tabPieces[tempX-1+i][tempY-1+j].getType() === 1 && this._tabPieces[tempX-1+i][tempY-1+j].getCouleur() != tour){
                 test = true;
                 resX = tempX-1+i;
                 resY = tempY-1+j;
               }
           }
       }
       return new Pos(resX, resY);
     }



    /*
    * renvoie true s'il y'a une pièce et qu'elle appartient au joueur courant à une position donnée
    *@param x pos en x de la pièce probable
    *@param y pos en y de la pièce probable
    *@param tour couleur du joueur courant
    *@return booleen
    */

    isOwn(x, y, tour) {
      return (this._tabPieces[x][y].getType() !== Type.VIDE && this._tabPieces[x][y].getCouleur() === tour);
    }

    /*
    * renvoie true s'il y'a une pièce et qu'elle appartient à l'adversaire à une position donnée
    *@param x pos en x de la pièce probable
    *@param y pos en y de la pièce probable
    *@param tour couleur du joueur courant
    *@return booleen
    */

    isOpp(x, y, tour) {
        return (this._tabPieces[x][y].getType() !== Type.VIDE && this._tabPieces[x][y].getCouleur() !== tour);
    }

    /**
     * echange la couleur et le type des 2 pièces
     * @param ancien la pièce à déplacer
     * @param nouveau la position de l'ancienne pièce
     */
    static echanger(ancien, nouveau) {
        // modif de la nouvelle couleur
        nouveau.setCouleur(ancien.getCouleur());
        nouveau.setType(ancien.getType());
        // nettoyage de l'ancienne position
        ancien.setCouleur(false);
        ancien.setType(Type.VIDE);
    }

    /**
     * getter du tableau de pièces
     * @returns {Array}
     */
    getTabPieces() {
        return this._tabPieces;
    }

    /**
    *modifie le tableau de pièces par un autre
    *@param Piece[][]
    */
    setTabPieces(newTab){
      this._tabPieces = newTab;
    }

    /**
    *retourne la position du Roi blanc
    *@return Pos
    */
    getPosRoiBlanc(){
      let i = 0;
      let j = 0;
      let trouve = false;
      let posRoiBlanc = new Pos(-1, -1);

      while(!trouve && j<8){
        if(this._tabPieces[i][j].getType() === Type.ROI && this._tabPieces[i][j].getCouleur() === true){
          posRoiBlanc = new Pos(i, j);
          trouve = true;
        }
        i++;
        if(i>7){
          i = 0;
          j++;
        }
      }
      if(posRoiBlanc === new Pos(-1, -1)){
        console.log("Le Roi blanc n'existe pas !");
      }
      return posRoiBlanc;
    }

    /**
    *retourne la position du Roi noir
    *@return Pos
    */
    getPosRoiNoir(){
      let i = 0;
      let j = 0;
      let trouve = false;
      let posRoiNoir = new Pos(-1, -1);

      while(!trouve && j<8){
        if(this._tabPieces[i][j].getType() === Type.ROI && this._tabPieces[i][j].getCouleur() === false){
          posRoiNoir = new Pos(i, j);
          trouve = true;
        }
        i++;
        if(i>7){
          i = 0;
          j++;
        }
      }
      if(posRoiNoir === new Pos(-1, -1)){
        console.log("Le Roi noir n'existe pas !");
      }
      return posRoiNoir;
    }


    /**
    *retourne le nombre de pièces présentes sur le Plateau
    *@return int
    */
    getNbrPieces(){
      let nbrPieces = 0;
      for(let i = 0; i< 8; i++){
        for(let j = 0; j<8; j++){
          if(this._tabPieces[i][j].getType() != 0){
            nbrPieces++;
          }
        }
      }
      return nbrPieces;
    }

    /**
    *retourne la position du pion si un privilège a été effectué, -1,-1 sinon
    *@return Pos
    */


    nettoyerProposition(){
        for(let i = 0 ; i < TAILLE ; ++i){
            for(let j = 0 ; j < TAILLE ; ++j){
                this._tabPieces[i][j].setEstCoupPossible(false);
            }
        }
    }


    ajoutPropostion(tabCoupsPossible){
        this.nettoyerProposition();
        // console.log(tabCoupsPossible);
        for(let i = 0 ; i < tabCoupsPossible.length ; ++i){
            this._tabPieces[tabCoupsPossible[i].x][tabCoupsPossible[i].y].setEstCoupPossible(true);
        }
    }
}
